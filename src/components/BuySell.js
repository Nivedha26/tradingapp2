import React,{Component} from 'react';
import axios from 'axios';
import {
    BrowserRouter as Router,
    Switch,
    Route,
    Link
  } from "react-router-dom";
export default class BuySell extends Component{
    
    constructor(props){
        super(props)
        this.state ={
            users:"",
            ticker:'',
            volume:''
        }
        
    }
   

    changeHandler=(e)  =>{
        this.setState({
            volume:e.target.value
      
            //[e.target.name]: e.target.value
        })
    }
    changeHandlerTicker=(e)  =>{
        this.setState({
         ticker:e.target.value
      
            //[e.target.name]: e.target.value
        })
    }


    submitBuyHandler = (e) => {
        e.preventDefault()
        axios.get(`http://localhost:8080/api/stocks/buy/${this.state.ticker}/${this.state.volume}`, this.state)
        .then(response => {
            console.log(response);
        })
        
    }
    submitSellHandler = (e) => {
        e.preventDefault()
        axios.get(`http://localhost:8080/api/stocks/sell/${this.state.ticker}/${this.state.volume}`, this.state)
        .then(response => {
            console.log(response);
        })
        
    }
    render()
    {
        return(
            <div>
                <form onSubmit={this.submitHandler}>
            <div>
            <input 
                id='ticker'
                type='text' 
                name ='ticker'
                placeholder='ticker' 
                value={this.state.ticker}
                onChange={this.changeHandlerTicker}
               ></input>
                <input 
                id='volume'
                type='text' 
                name ='volume'
                placeholder='Volume' 
                value={this.state.volume}
                onChange={this.changeHandler}
               ></input>

            </div>
            <button onClick={this.submitBuyHandler}>Buy</button>
            <button onClick={this.submitSellHandler}>Sell</button>
            </form>
            <div>{this.state.users}</div>   
            </div>
        );
    }

}