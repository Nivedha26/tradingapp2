import React, { useState, useEffect } from "react";
import { Bar, Line, Pie } from 'react-chartjs-2';
import "./HomeBar.css";
import axios from "axios";
const HomeGraph = () => {
  const [chartData, setChartData] = useState({});
  const [employeeSalary, setEmployeeSalary] = useState([]);
  const [employeeAge, setEmployeeAge] = useState([]);
  const chart = () => {
    let empRet= [];
    let empTic=[];
    axios
      .get("http://localhost:8080/api/stocks/update")
      .then(res => {
        console.log(res);
        for (const dataObj of res.data) {
          empRet.push(parseInt(dataObj.netAssetValue));
          empTic.push(dataObj.ticker);
        }
        setChartData({
          labels: empTic,
          
          datasets: [
            {
              label: "Net Assets",
              data: empRet,
              backgroundColor: ["#0074D9", "#FF4136", "#2ECC40", "#FF851B", "#7FDBFF", "#B10DC9", "#FFDC00", "#001f3f", "#39CCCC", "#01FF70", "#85144b", "#F012BE", "#3D9970", "#111111", "#AAAAAA"],
        
          
              borderWidth: 15,
              
            }
          ]
        });
      })
      .catch(err => {
        console.log(err);
      });
    console.log(empTic, empRet);
  };
  useEffect(() => {
    chart();
  }, []);
  return (
    <div className="App">
      <h1 className="text-center">Net Assets </h1>
      <div class="graph">
     
        <Pie className="asset"
          data={chartData}
          options={{
            responsive: true,
          
            title:{
                display: true,
                text: "Color test"
            }
    
            
            }
          }
        />
        
      </div>
    </div>
  );
};
export default HomeGraph;
