import React from 'react'
import "./sidebar.css"
import {
    Timeline,
    LineStyle,
    TrendingUp,
  } from "@material-ui/icons";

export default class Sidebar extends React.Component{
    render(){
    return (
        <div className="sidebar">
      <div className="sidebarWrapper">
        <div className="sidebarMenu">
          <h3 className="sidebarTitle">Menu</h3>
          <ul className="sidebarList"></ul>
          <li className="sidebarListItem active">
              <LineStyle  class="sidebarIcon"/>Home 
          </li>
          <li className="sidebarListItem">
              <Timeline class="sidebarIcon"/>Trade History 
          </li>
          <li className="sidebarListItem">
              <TrendingUp class="sidebarIcon"/> Portfolio 
          </li>
          </div>
          </div>
          </div>
    )
    }
}
