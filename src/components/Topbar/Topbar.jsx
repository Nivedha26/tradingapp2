import React from 'react'
import "./topbar.css"

export default class Topbar extends React.Component {
    render(){
        return (
        
            <div className="topbar">
                <div className="topbarWrapper">
                    <div className="topLeft">
                        <span className="logo">Dashboard</span>
                    </div>
                <div className="topRight">
                    <img src="https://image.flaticon.com/icons/png/512/1177/1177568.png" alt="" className="topAvatar"/>
                </div>
                </div>
            </div>
        
    );
    }
}
