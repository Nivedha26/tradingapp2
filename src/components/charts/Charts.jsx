import React from 'react';
import "./charts.css";
import {
  LineChart,
  Line,
  XAxis,
  CartesianGrid,
  Tooltip,
  ResponsiveContainer,
  YAxis,
} from "recharts";
import axios from 'axios';
export default function Chart(grid) {

  let empRet = [];
    let empTic = [];
    axios
      .get("http://localhost:8080/api/stocks/update")
      .then(res => {
        console.log(res);
        console.log(res.ticker);
        for (const dataObj of res.data) {
          empRet.push(dataObj.returns);
          empTic.push(dataObj.ticker);
        }});
  return (
    <div className="chart">
      <h3 className="chartTitle">Analysis</h3>
      <ResponsiveContainer  aspect={ 4/ 1}>
        <LineChart  margin={{
        top: 10,
        right: 30,
        left: 70,
        bottom: 20
      }}>
          <XAxis dataKey={empTic} stroke="#5550bd" />
          <YAxis dataKey={empRet} stroke="#5550bd" />
          <Line type="monotone" dataKey={empRet} stroke="#5550bd" />
          <Tooltip />
          {grid && <CartesianGrid stroke="#e0dfdf" strokeDasharray="5 5" />}
        </LineChart>
      </ResponsiveContainer>
    </div>
  );
}
