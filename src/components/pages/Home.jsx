import React from 'react';
import Featuredinfo from "../featuredinfo/Featuredinfo";
import Charts from "../charts/Charts";
import "./Home.css";
import PieChart from '../Piechart';
import HomeGraph from '../HomeGraph';


export default class Home extends React.Component{
    render(){
    return (
        <div className="home">
            <Featuredinfo />
            <HomeGraph/>
        </div>
    );
    }
}

