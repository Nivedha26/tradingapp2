import React,{useState} from "react";
import "./order.css";

export default function Order() {
    const [values,setValues]=useState({
        ticker:"",
        vol:""
    });

    const [submitted, setSubmitted] =useState(false);
    const [valid, setValid] =useState(false);
    
    const handleticker = (event) => {
        setValues({...values,ticker: event.target.value})
    }
    const handlevol = (event) => {
        setValues({...values,vol: event.target.value})
    }
    const handleSubmit =(event) =>
    {
        event.preventDefault();
        if(values.ticker && values.vol){
            setValid(true);
        }
        setSubmitted(true);
    }
  return (
    <div class="form-container">
        <div>Stock Details</div>
      <form class="register-form" onSubmit={handleSubmit}>
        
        <input
        onChange={handleticker}
          value={values.ticker}
          id="ticker"
          class="form-field"
          type="text"
          placeholder="Ticker"
          name="ticker"
        />
        
        {submitted && !values.ticker ? <span id="first-name-error">Please enter ticker</span> : null}
        <input
          //id="vol"
          onChange={handlevol}
          value={values.vol}
          class="form-field"
          type="text"
          placeholder="Volume"
          name="vol"
        />
        
        {submitted && !values.vol ? <span id="first-name-error">Please enter volume</span> : null}

        {submitted  && valid ? <div class="success-message">Success! Thank you for registering</div> : null}
        <button class="buy" type="submit">Buy</button>
        <button class="sell" type="submit">Sell</button>
      </form>
    </div>
  );
}