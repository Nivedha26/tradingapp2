import React from 'react';
import userservice from '../services/UserServices';
import 'bootstrap/dist/css/bootstrap.min.css';
import {Navbar, Nav,Form,FormControl,Button}  from 'react-bootstrap';

class UserComponents extends React.Component{

    constructor(props){
        super(props)
        this.state ={
            users:[]
        }
        
    }
    componentDidMount(){
        userservice.getUsers().then((Response) =>{
            this.setState({users:Response.data})
        });
    }
    render(){
        return(
            <div>
                
                <h1 className = "text-center">Trade History</h1>
                <table className = "table table-striped">
                    <thead>
                        <tr>
                            <th>Order Id</th>
                            <th>Ticker</th>
                            <th>DateTime</th>
                            <th>Volume</th>
                            <th>Value</th>
                            <th>Buy/Sell</th>

                        </tr>
                    </thead>

                    <tbody>
                        {
                            this.state.users.map(
                                user =>
                                <tr key = {user.orderId}>
                                    <td>{user.orderId}</td>
                                    <td>{user.ticker}</td>
                                    <td>{user.dateTime}</td>
                                    <td>{user.volume}</td>
                                    <td>{user.valuePerStock}</td>
                                    <td>{user.buyOrSell}</td>
                                </tr>
                            )
                            }
                    </tbody>
                </table>
            </div>
        )
    }
}
export default UserComponents