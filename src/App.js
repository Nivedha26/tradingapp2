//import logo from './logo.svg';
import Sidebar from "./components/sidebar/Sibebar.jsx";
import Topbar from "./components/Topbar/Topbar";
import Home from "./components/pages/Home";
import History from "./components/pages/History";
import Portfolio from "./components/Portfolio";
import Navbar from "./components/Navbar";
import './App.css';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from "react-router-dom";
import UserComponents from '../src/components/UserComponent';
import Order from './components/DummyFile/Order';
import Explore from './components/Explore';
import BuySell from './components/BuySell';
import PortfolioService from './components/PortfolioService';

function App() {
  return (
    <>
    <Router>
        <Navbar />
        <Switch>
          <Route path='/' exact component={Home} />
          <Route path='/explore' component={Explore} />
          <Route path='/history' component={History} />
          <Route path='/portfolio' component={Portfolio} />
          
          
        </Switch>
      </Router>
      </>
    //<div className="App">
    
      /*  <Router >
     To check buy or sell <Route path='/portfolio' component={PostForm} />
      <Topbar/>
      
      <div className="container">
      <Sidebar/>
        <Switch>
          <Route exact path='/'> 
            <Home/>
          </Route>
          <Route path='/tradeHistory'> 
            <History/>
          </Route>
          <Route path='/portfolio'> 
            <Portfolio/>
          </Route>
        </Switch>
      </div>
      </Router>*/

      
  
    /*<div className="App">
    <Router >
      <Topbar/>
      <div className="container">
        <Sidebar/>
        <Switch>
          <Route exact path='/'> 
            <Home/>
          </Route>
          <Route path='/tradeHistory'> 
            <History/>
          </Route>
        </Switch>
      </div>
      </Router>
      <UserComponents/>
      <Topbar/>
          </div>*/
          
  );
}

export default App;
